//
//  GenericInformationViewController.m
//  Journal
//
//  Created by Michael Childs on 2/8/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "GenericInformationViewController.h"

@interface GenericInformationViewController ()

@end

@implementation GenericInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    
    UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(btnTouched:)];
    
    NSArray *actionButtonItems = @[saveBtn];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    
    tv = [[UITextView alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, self.view.frame.size.height/2 - 20)];
    tv.delegate = self;
    tv.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:tv];
    
    
    //    display "Done Editing" button above keyboard
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done Editing" forState:UIControlStateNormal];
    [btn setTintColor:[UIColor whiteColor]];
    btn.frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    [btn addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor grayColor]];
    
    tv.inputAccessoryView = btn;
    
    
    if (self.dayJournalDictionary) {
        tv.text = [self.dayJournalDictionary objectForKey:@"journalText"];
    }
    
}

-(void)doneEditing:(id)sender {      //     put keyboard away
    [self.view endEditing:YES];
    
}

-(void)btnTouched:(id)sender {
    
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if (!self.dayJournalDictionary) {
        self.dayJournalDictionary = [NSMutableDictionary new];
        [self.dayJournalDictionary setObject:tv.text forKey:@"journalText"]; // display object(journalText) when user taps on corresponding entry
        
        [ad.journalDataArray addObject:self.dayJournalDictionary];
        
        
    } else {
        self.dayJournalDictionary [@"journalText"] = tv.text;//setValue:tv.text forKey:@"journalText"];
        [ad.journalDataArray replaceObjectAtIndex:self.dayJournalIndexPath.row withObject:self.dayJournalDictionary];
    }
    
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];       //instantiate use of NSUserDefaults in order to save user's journal entries
    [defaults setObject:ad.journalDataArray forKey:@"dataArray"];   // display entry names in tableView
    [defaults synchronize];                                         //synchronize and save text entered by user
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    tv.frame = CGRectMake(10, 10, self.view.frame.size.width - 20, self.view.frame.size.height/2 - 20);     // display journal entry "canvas" or textView as a smaller rectangle while editing text
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    tv.frame = CGRectMake(10, 10, self.view.frame.size.width - 20, self.view.frame.size.height - 20);       // display journal canvas as complete entry, filling the screen, when user is done editing
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
