//
//  AppDelegate.h
//  Journal
//
//  Created by Michael Childs on 2/8/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootTableViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>


{
    NSUserDefaults* defaults;
}


@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray* journalDataArray;


@end

