//
//  RootTableViewController.h
//  Journal
//
//  Created by Michael Childs on 2/8/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericInformationViewController.h"

@interface RootTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    UITableView* mytableView;
    NSMutableArray* array;
}

@end
