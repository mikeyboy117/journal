//
//  GenericInformationViewController.h
//  Journal
//
//  Created by Michael Childs on 2/8/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface GenericInformationViewController : UIViewController<UITextViewDelegate>

{
    UITextView* tv;
}

@property (nonatomic, strong) NSMutableDictionary* dayJournalDictionary;
@property (nonatomic, strong) NSIndexPath* dayJournalIndexPath;

@end
