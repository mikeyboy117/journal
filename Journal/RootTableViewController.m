//
//  RootTableViewController.m
//  Journal
//
//  Created by Michael Childs on 2/8/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "RootTableViewController.h"
#import "AppDelegate.h"

@interface RootTableViewController ()

@end

@implementation RootTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem* addNewBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(btnTouched:)];
    
    NSArray *actionButtonItems = @[addNewBtn];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    //create table view to display list of journal entries
    
    mytableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    
    //    not sure what these mean
    mytableView.delegate = self;
    mytableView.dataSource = self;
    [self.view addSubview:mytableView];
    
    
    self.navigationItem.title = @"My Journal";          //  display title in tableView Navigation Bar center
    
}

-(void)viewWillAppear:(BOOL)animated {
    [self loadData];
}

-(void)loadData {
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    array = ad.journalDataArray;
    
    
    [mytableView reloadData];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {       // only display
    return array.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    NSMutableDictionary* journalText = [array objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [journalText objectForKey:@"journalText"];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    GenericInformationViewController* givc = [GenericInformationViewController new];
    givc.dayJournalDictionary = [[array objectAtIndex:indexPath.row] mutableCopy];
    givc.dayJournalIndexPath = indexPath;
    [self.navigationController pushViewController:givc animated:YES];
    
}




-(void)addNewBtnTouched:(id)sender {
    NSLog(@"Touched");
}

-(void)btnTouched:(id)sender {
    
    GenericInformationViewController* givc = [GenericInformationViewController new];
    
    [self.navigationController pushViewController:givc animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
