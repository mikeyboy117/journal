//
//  ViewController.m
//  Journal
//
//  Created by Michael Childs on 2/8/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    RootTableViewController* rtvc = [RootTableViewController new];
    
    UINavigationController* nc = [[UINavigationController alloc]initWithRootViewController:rtvc];
    [nc.navigationBar setHidden:NO];
    
    [nc.navigationBar setTranslucent:YES];           //display nav bar either translucent or opaque
    [self addChildViewController:nc];
    [self.view addSubview:nc.view];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
